package data

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"gitlab.com/armashka/greenlight.git/internal/validator"
)

type House struct {
	ID             int64     `json:"id"`
	CreatedAt      time.Time `json:"-"`
	Name           string    `json:"name"`
	Year           int32     `json:"year,omitempty"`
	ApartmentQuant Aparts    `json:"apartment_quantity,omitempty"`
	Address        string    `json:"address,omitempty"`
	Version        int32     `json:"version"`
}

func ValidateHouse(v *validator.Validator, house *House) {
	v.Check(house.Name != "", "name", "must be provided")
	v.Check(len(house.Name) <= 500, "name", "must not be more than 500 bytes long")
	v.Check(house.Year != 0, "year", "must be provided")
	v.Check(house.Year >= 1600, "year", "must be greater than 1600")
	v.Check(house.Year <= int32(time.Now().Year()), "year", "must not be in the future")
	v.Check(house.ApartmentQuant != 0, "apartment_quantity", "must be provided")
	v.Check(house.ApartmentQuant > 0, "apartment_quantity", "must be a positive integer")
	v.Check(house.Address != "", "address", "must be provided")
}

type HouseModel struct {
	DB *sql.DB
}

func (h HouseModel) Insert(house *House) error {

	query := `
	INSERT INTO houses (name, year, apartment_quantity, address)
	VALUES ($1, $2, $3, $4)
	RETURNING id, created_at, version`

	args := []interface{}{house.Name, house.Year, house.ApartmentQuant, house.Address} // beautiful
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	return h.DB.QueryRowContext(ctx, query, args...).Scan(&house.ID, &house.CreatedAt, &house.Version)
}

func (h HouseModel) Get(id int64) (*House, error) {
	if id < 1 {
		return nil, ErrRecordNotFound
	}

	query := `
		SELECT id, created_at, name, year, apartment_quantity, address, version
		FROM houses
		WHERE id = $1`

	var house House
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	err := h.DB.QueryRowContext(ctx, query, id).Scan(&house.ID,
		&house.CreatedAt,
		&house.Name,
		&house.Year,
		&house.ApartmentQuant,
		&house.Address,
		&house.Version,
	)

	if err != nil {
		switch {
		case errors.Is(err, sql.ErrNoRows):
			return nil, ErrRecordNotFound
		default:
			return nil, err
		}
	}
	return &house, nil

}

func (h HouseModel) Update(house *House) error {
	query := `
			UPDATE houses
			SET name = $1, year = $2, apartment_quantity = $3, address = $4, version = version + 1 
			WHERE id = $5 AND version = $6
			RETURNING version`

	args := []interface{}{house.Name,
		house.Year,
		house.ApartmentQuant,
		house.Address,
		house.ID,
		house.Version, // Add the expected house version.
	}

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	err := h.DB.QueryRowContext(ctx, query, args...).Scan(&house.Version)
	if err != nil {
		switch {
		case errors.Is(err, sql.ErrNoRows):
			return ErrEditConflict
		default:
			return err
		}
	}

	return nil
}

func (h HouseModel) Delete(id int64) error {
	if id < 1 {
		return ErrRecordNotFound
	}

	query := `DELETE FROM houses WHERE id = $1`
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	result, err := h.DB.ExecContext(ctx, query, id)
	if err != nil {
		return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrRecordNotFound
	}

	return nil

}

func (h HouseModel) GetAll(name string, address string, filters Filters) ([]*House, Metadata, error) {

	query := fmt.Sprintf(`
		SELECT count(*) OVER(), id, created_at, name, year, apartment_quantity, address, version FROM houses
		WHERE (to_tsvector('simple', name) @@ plainto_tsquery('simple', $1) OR $1 = '') AND (to_tsvector('simple', address) @@ plainto_tsquery('simple', $2) OR $2 = '')
		ORDER BY %s %s, id ASC
		LIMIT $3 OFFSET $4`, filters.sortColumn(), filters.sortDirection())

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	args := []interface{}{name, address, filters.limit(), filters.offset()}

	rows, err := h.DB.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, Metadata{}, err
	}

	defer rows.Close()

	totalRecords := 0
	houses := []*House{}
	for rows.Next() {

		var house House

		err := rows.Scan(
			&totalRecords,
			&house.ID,
			&house.CreatedAt,
			&house.Name,
			&house.Year,
			&house.ApartmentQuant,
			&house.Address,
			&house.Version,
		)
		if err != nil {
			return nil, Metadata{}, err
		}
		houses = append(houses, &house)

	}
	if err = rows.Err(); err != nil {
		return nil, Metadata{}, err
	}

	metadata := calculateMetadata(totalRecords, filters.Page, filters.PageSize)

	return houses, metadata, nil
}
