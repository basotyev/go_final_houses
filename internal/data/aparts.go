package data

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type Aparts int32

var ErrInvalidaApartmentQuantFormat = errors.New("invalid quantity format")

func (a Aparts) MarshalJSON() ([]byte, error) {
	jsonValue := fmt.Sprintf("%d aparts", a)
	quotedJSONValue := strconv.Quote(jsonValue)
	return []byte(quotedJSONValue), nil
}
func (a *Aparts) UnmarshalJSON(jsonValue []byte) error {
	unquotedJSONValue, err := strconv.Unquote(string(jsonValue))
	if err != nil {
		return ErrInvalidaApartmentQuantFormat
	}
	parts := strings.Split(unquotedJSONValue, " ")
	if len(parts) != 2 || parts[1] != "aparts" {
		return ErrInvalidaApartmentQuantFormat
	}
	i, err := strconv.ParseInt(parts[0], 10, 32)
	if err != nil {
		return ErrInvalidaApartmentQuantFormat
	}
	*a = Aparts(i)
	return nil
}
