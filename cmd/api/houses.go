package main

import (
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/armashka/greenlight.git/internal/data"
	"gitlab.com/armashka/greenlight.git/internal/validator"
)

func (app *application) showHouseHandler(w http.ResponseWriter, r *http.Request) {

	id, err := app.readIDParam(r)
	if err != nil || id < 1 {
		app.notFoundResponse(w, r)
		return
	}

	movie, err := app.models.Houses.Get(id)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	err = app.writeJSON(w, http.StatusOK, envelope{"house": movie}, nil)
	if err != nil {
		app.logError(r, err)
		app.serverErrorResponse(w, r, err)
	}

}

func (app *application) createHouseHandler(w http.ResponseWriter, r *http.Request) {

	// DTO to safe ID and Version fields
	var input struct {
		Name           string      `json:"name"`
		Year           int32       `json:"year"`
		ApartmentQuant data.Aparts `json:"apartment_quantity"`
		Address        string      `json:"address,omitempty"`
	}

	err := app.readJSON(w, r, &input)

	if err != nil {
		app.badRequestResponse(w, r, err)
		return
	}

	v := validator.New()

	house := &data.House{
		Name:           input.Name,
		Year:           input.Year,
		ApartmentQuant: data.Aparts(input.ApartmentQuant),
		Address:        input.Address,
	}

	if data.ValidateHouse(v, house); !v.Valid() {
		app.failedValidationResponse(w, r, v.Errors)
		return
	}

	err = app.models.Houses.Insert(house)
	if err != nil {
		app.serverErrorResponse(w, r, err)
		return
	}

	headers := make(http.Header)
	headers.Set("Location", fmt.Sprintf("/v1/movies/%d", house.ID))

	err = app.writeJSON(w, http.StatusCreated, envelope{"house": house}, headers)
	if err != nil {
		app.serverErrorResponse(w, r, err)
	}

}

func (app *application) updateHouseHandler(w http.ResponseWriter, r *http.Request) {
	id, err := app.readIDParam(r)

	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	house, err := app.models.Houses.Get(id)

	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	var input struct {
		Name           *string      `json:"name"`
		Year           *int32       `json:"year"`
		ApartmentQuant *data.Aparts `json:"apartment_quantity"`
		Address        *string      `json:"address,omitempty"`
	}

	err = app.readJSON(w, r, &input)
	if err != nil {
		app.badRequestResponse(w, r, err)
		return
	}
	if input.Name != nil {
		house.Name = *input.Name
	}

	if input.Year != nil {
		house.Year = *input.Year
	}

	if input.ApartmentQuant != nil {
		house.ApartmentQuant = *input.ApartmentQuant
	}
	if input.Address != nil {
		house.Address = *input.Address // Note that we don't need to dereference a slice.
	}

	v := validator.New()

	if data.ValidateHouse(v, house); !v.Valid() {
		app.failedValidationResponse(w, r, v.Errors)
		return
	}

	err = app.models.Houses.Update(house)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrEditConflict):
			app.editConflictResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	err = app.writeJSON(w, http.StatusOK, envelope{"house": house}, nil)
	if err != nil {
		app.serverErrorResponse(w, r, err)
	}

}

func (app *application) deleteHouseHandler(w http.ResponseWriter, r *http.Request) {
	id, err := app.readIDParam(r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	err = app.models.Houses.Delete(id)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	err = app.writeJSON(w, http.StatusOK, envelope{"message": "house successfully deleted"}, nil)
	if err != nil {
		app.serverErrorResponse(w, r, err)
	}

}

func (app *application) listHouseHandler(w http.ResponseWriter, r *http.Request) {

	var input struct {
		Name    string
		Address string
		data.Filters
	}

	v := validator.New()

	qs := r.URL.Query()

	input.Name = app.readString(qs, "name", "")
	input.Address = app.readString(qs, "address", "")

	input.Filters.Page = app.readInt(qs, "page", 1, v)
	input.Filters.PageSize = app.readInt(qs, "page_size", 20, v)
	input.Filters.Sort = app.readString(qs, "sort", "id")
	input.Filters.SortSafelist = []string{"id", "name", "year", "apartment_quantity", "-id", "-name", "-year", "-apartment_quantity"}

	if data.ValidateFilters(v, input.Filters); !v.Valid() {
		app.failedValidationResponse(w, r, v.Errors)
		return
	}

	movies, metadata, err := app.models.Houses.GetAll(input.Name, input.Address, input.Filters)
	if err != nil {
		app.serverErrorResponse(w, r, err)
		return
	}

	err = app.writeJSON(w, http.StatusOK, envelope{"houses": movies, "metadata": metadata}, nil)
	if err != nil {
		app.serverErrorResponse(w, r, err)
	}
}
