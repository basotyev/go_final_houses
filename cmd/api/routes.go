package main

import (
	"expvar"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func (app *application) routes() http.Handler {
	router := httprouter.New()

	router.NotFound = http.HandlerFunc(app.notFoundResponse)
	router.MethodNotAllowed = http.HandlerFunc(app.methodNotAllowedResponse)

	router.HandlerFunc(http.MethodGet, "/v1/healthcheck", app.healthcheckHandler)

	router.HandlerFunc(http.MethodGet, "/v1/houses", app.requirePermission("houses:read", app.listHouseHandler))
	router.HandlerFunc(http.MethodPost, "/v1/houses", app.requirePermission("houses:write", app.createHouseHandler))
	router.HandlerFunc(http.MethodGet, "/v1/houses/:id", app.requirePermission("houses:read", app.showHouseHandler))
	router.HandlerFunc(http.MethodPatch, "/v1/houses/:id", app.requirePermission("houses:write", app.updateHouseHandler))
	router.HandlerFunc(http.MethodDelete, "/v1/houses/:id", app.requirePermission("houses:write", app.deleteHouseHandler))

	router.HandlerFunc(http.MethodPost, "/v1/users", app.registerUserHandler)
	router.HandlerFunc(http.MethodPut, "/v1/users/activated", app.activateUserHandler)

	router.HandlerFunc(http.MethodPost, "/v1/tokens/authentication", app.createAuthenticationTokenHandler)

	router.Handler(http.MethodGet, "/debug/vars", expvar.Handler())

	return (app.recoverPanic(app.enableCORS(app.rateLimit(app.authenticate(router)))))
}
