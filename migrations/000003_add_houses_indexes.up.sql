CREATE INDEX IF NOT EXISTS houses_name_idx ON houses USING GIN (to_tsvector('simple', name));
CREATE INDEX IF NOT EXISTS houses_address_idx ON houses USING GIN (to_tsvector('simple', address));