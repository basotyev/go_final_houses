
CREATE TABLE IF NOT EXISTS houses (
    id bigserial PRIMARY KEY,
    created_at timestamp(0) with time zone NOT NULL DEFAULT NOW(), 
    name text NOT NULL,
    year integer NOT NULL,
    apartment_quantity integer NOT NULL,
    address text NOT NULL,
    version integer NOT NULL DEFAULT 1
);