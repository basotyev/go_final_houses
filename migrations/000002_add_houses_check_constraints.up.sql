ALTER TABLE houses ADD CONSTRAINT houses_apartments_check CHECK (apartment_quantity >= 0);
ALTER TABLE houses ADD CONSTRAINT houses_year_check CHECK (year BETWEEN 1600 AND date_part('year', now()));