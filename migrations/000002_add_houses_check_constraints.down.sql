ALTER TABLE houses DROP CONSTRAINT IF EXISTS houses_apartments_check;
ALTER TABLE houses DROP CONSTRAINT IF EXISTS houses_year_check;